const express = require("express");

const app = express();
app.use(express.json());

const scores = [];

app.get("/scores", (req, res) =>
{
    res.status(500).send("not implemented yet.");
});

app.post("/scores", (req, res) =>
{
    // score: {initials: "", points: 1000}
    const score = req.body;
    scores.push(score);
    res.status(201).send("score added.");
});

app.listen(3000, ()=>console.log("app started"));